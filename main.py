import math

rib_length = int(input("enter the length of the rib\n"))

number_to_multiply_spaces = 1
if rib_length < 4:
    number_to_multiply_spaces = 0
elif rib_length < 7:
    number_to_multiply_spaces = int(rib_length / 2)
elif rib_length < 10:
    number_to_multiply_spaces = int(math.sqrt(rib_length)) * 2
elif rib_length < 13:
    number_to_multiply_spaces = int(math.sqrt(rib_length)) * 3

print("  *       *")
print(" ***     ***")
print("*****   *****")


matmon_line = "*" + " " * number_to_multiply_spaces + \
    "Matmon" + " " * number_to_multiply_spaces + "*"
print(matmon_line)

for i in range(1, rib_length - 1):
    for j in range(i):
        print(" ", end="")
    print("*", end="")
    for k in range(2 * (rib_length - i)):
        print(" ", end="")
    print("*", end="")
    print()

lower_line = " " * (rib_length) + " *"
print(lower_line)
